import socket
import select
import re
import json
import threading
import queue
from collections import OrderedDict

#fonts close to q3 https://fonts.google.com/specimen/Josefin+Sans + https://fonts.google.com/specimen/Ropa+Sans
#helpful links https://github.com/multiplay/qstat/blob/2ab78bd5c30fbd47b2bdd4a6279296d66424b37e/qstatdoc.html https://github.com/boundary/wireshark/blob/master/epan/dissectors/packet-quake3.c https://github.com/multiplay/qstat/


def qcolors_to_html(text):
    # replace all 3 color text with html
    a = re.sub(r"\^x([0-9A-z]{3})",r'</span><span style="color:#\1">',text)
    # replace the single char colors with html
    scolors = ['black','red','green','yellow','blue','aqua','pink','white','grey','gray']
    a = re.sub(r"\^([0-9])",lambda m: '</span><span style="color:'+scolors[int(m.group()[1])]+'">', a)
    # remove all the extra spans
    a = re.sub(r"(<\/span>)",r'',a,1)
    # add </span> to any span that is open (use \b if bug)
    a = re.sub(r"<span style=\"color:[a-z]*\">(.*)",r'\1'+"</span>",a)
    # add a closing span if there is a span open
    return a

def qemoji_to_html(text):
    text = re.sub(r"(:?\\\\x)([A-z0-9]{2})",r'\2',text)
    text = bytes.fromhex(text).decode('utf-8')
    return text


def qhex_to_html(text):
    # de/encode utf-8 emojis which are 4 hex values long
    text = re.sub(r"\\\\xf0(\\\\x[A-z0-9]{2}){3}", lambda m: qemoji_to_html(m.group()), text)
    # de/encode quakefont which are 3 hex values long
    text = re.sub(r"(\\\\x([A-z0-9]{2})){3}", lambda m: qemoji_to_html(m.group()), text)
    return text

def qtext(text):
    text = qcolors_to_html(text)
    text = qhex_to_html(text)
    return text

def qhex_to_ascii(text):
    print("hex="+text)
    #print(bytes.fromhex(text).decode('cp1252'))
    print(bytes.fromhex(text))
    #print(int(text,16))
    
    return text

# Connect to master server and retirve server list
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.connect(("dpmaster.deathmask.net",27950)) #others: "107.161.23.68",27950 "192.0.2.22",26002
sock.setblocking(0)
sock.settimeout(4)
# Ask master server for serverlist
sock.send(b"\xFF\xFF\xFF\xFFgetservers Xonotic 3 empty full\n")
# recieve the response turn it into hex
data = sock.recv(4096).hex()
# split up IP and PORT
data = re.findall(r"5c([0-9A-z]{8})([0-9A-z]{4})", data)
servers = [] # {"ip",port}
for server in data:
    #print(server)
    host = server[0]
    #print(host)
    ip = ""
    # convert every 2 character hex values to an int
    for x in [host[0:2],host[2:4],host[4:6],host[6:8]]:
        #print(x)
        ip += repr(int(x,16)) +"."
    ip = ip[:-1]
    port = int(server[1],16)
    servers += [(ip,port)]
#print(servers)
#print(int(data.hex(),16))
sock.close

# query and parse getStatus server info
def getServerInfo(ip,port):
    #print('getserverinfo: '+ip)
    #print(port)
    server = None
    try:
        # create a udp connection to the server
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP socket
        sock.connect((ip,port))
        sock.setblocking(0)
        sock.settimeout(5)
        # query the server
        sock.send(b'\xFF\xFF\xFF\xFFgetstatus')
        data = repr(sock.recv(1024))
        sock.close()
        # create the server{} dictionary
        server = {"hostname":"","ip":"","port":"","mapname":"","clients":"","maxclients":"","gametype":"","bots":"","players":{},}
        server["ip"]=ip
        server["port"]=port
        # single line regex matches
        server["mapname"] = re.search(r"\\\\mapname\\\\(.+?)\\\\", data)[1]
        server["hostname"] = qtext(repr(re.search(r"\\\\(sv_)?hostname\\\\(.+?)\\\\", data)[2]))[1:-1]
        server["maxclients"] = re.search(r"\\\\sv_maxclients\\\\(.+?)\\\\", data)[1]
        server["clients"] = re.search(r"\\\\clients\\\\(.+?)\\\\", data)[1]
        server["gametype"] = re.search(r"\\\\qcstatus\\\\(.+?)\:", data)[1]
        server["bots"] = re.search(r"\\\\bots\\\\(.+?)\\\\", data)[1]
        # grab every player entry and cycle through each one
        players = re.finditer(r"\\n([-0-9].+?\".+?[\"])", data)
        server["players"] = []
        for p in players:
            p = repr(p.group(1))
            score = re.search(r"([-0-9]+) |\".+?\"", p).group(1)
            ping = re.search(r" ([0-9]+) |\".+?\"", p).group(1)
            name = qtext(re.search(r"\"(.+?)\"", p).group(1))
            server["players"].append( ({"name":name,"ping":ping,"score":score}) )
            #server["players"].append( [{"name":name,"ping":ping,"score":score}] )
        """if server["players"]:
        #if True:
            print("sorting")
            print(server["players"])
            #sorted(server["players"], key = lambda i: i["score"])
            #server['players'].sort(key = itemgetter((['players']['score'])), reverse = false)
            #for k in sorted(server['players']): print(k)
            #print("sorted:")
            #print(server["players"])
        print(server['players'])"""
    except Exception as e: 
        #print('function error')
        print(e)
        return None
    if server != None:
        #print(server)
        return server

    #print("\n")
    #servers += json.dumps(server)
    #print(json.dumps(server))

# queue init for server queries
my_queue = queue.Queue()

def storeInQueue(f):
  def wrapper(*args):
    my_queue.put(f(*args))
  return wrapper

# run all the server querys in a queue
@storeInQueue
def qGetThread(ip,port):
    #print( getServerInfo(ip,port) )
    global serverlist
    server = getServerInfo(ip,port)
    if server != None:
        serverlist.append(server)
    #return getServerInfo(ip,port)

def getServerlist():
    global serverlist
    threads = []
    for x in servers:  
        #print("requesting info from:"+x[0])
        #threading.Thread(target = serverlist.append, args = ( getServerInfo(x[0],x[1]) ) ).start()
        # use a list of threads to ping EVERY SERVER AT ONCE
        t = threading.Thread(target = qGetThread, args = (x[0], x[1] ) )
        threads.append(t)
        #qGetThread(x[0], x[1] )
        # print("serverlist:")
        #print(serverlist)        

    # start and stop all the ping threads
    for t in threads: t.start()
    for t in threads: t.join()

# sort serverlist by servers with most players
def extract_players(json):
    try:
        return int(len(json['players']))
    except KeyError:
        return 0

# lines.sort() is more efficient than lines = lines.sorted()
def serverlistExport():
    global serverlist
    getServerlist()
    serverlist.sort(key=extract_players, reverse=True)

    # create and write server response to json file
    serverlist =json.dumps(serverlist)
    print("serverlist:")
    print(serverlist)
    #print(serverlist)
    #html+="<b/></body></html>"
    text_file = open("servers.json", "w", encoding="utf-8")
    text_file.write(serverlist)
    text_file.close()

from time import sleep
while True:
    try:
        serverlist = []
        serverlistExport()
        sleep(2)
    except KeyError:
        print('a')